# Import SMTK template file (.sbt) and generate simput model

import argparse
import json
import os
import sys


class Sbt2Simput:
    """"""
    def __init__(self):
        self.att_resource = None
        self.simput_model = dict()
        self.simput_views = dict()
        self.value_type_map= {
            smtk.attribute.Item.DoubleType: 'double',
            smtk.attribute.Item.IntType: 'int',
            smtk.attribute.Item.StringType: 'string',
        }
        self.view_type_map = {
            'Instanced': 'InstancedAttributes',
            'Attribute': 'AttributeTypes'
        }

    def convert(self, att_resource):
        """Traverses attribute resource and generates Simput model."""
        self.att_resource = att_resource
        self.simput_model.clear()
        self.simput_views.clear()

        self._convert_definitions()
        self._convert_views()

        return self.simput_model

    def _add_name_parameter(self, att_type_list, view_dict):
        """And "Name" parameter to attribute types in the list

        If the list has one item, also add hook to set the attribute name as well.
        This method should only be called after all definitions have been processed (obviously).
        """
        defn_dict = self.simput_model.get('definitions')
        if not defn_dict:
            print('WARNING: definitions dict is empty')

        for att_type in att_type_list:
            defn = defn_dict.get(att_type)
            if defn is None:
                print('WARNING missing definition type {}'.format(att_type))
                continue
            defn_params = defn.get('parameters', [])
            # Check that _name not already inserted
            if defn_params and defn_params[0].get('id') == '_name':
                continue
            name_dict = dict(id='_name', label='Name', size=1, type='string')
            defn_params.insert(0, name_dict)

        if len(att_type_list) == 1:
            att_type = att_type_list[0]
            hook_name = '{}._name'.format(att_type)
            hook = dict(type='copyParameterToViewName', attribute=hook_name)
            view_dict['hooks'] = [hook]

    def _convert_definition(self, defn, show=None):
        """Converts attribute or group-item definition.

        Args:
            defn: AttributeDefinition or GroupItemDefintiion
        Returns:
            list of Simput parameters converted from defn (could be empty)
        """
        param_list = list()
        for i in range(defn.numberOfItemDefinitions()):
            item_def = defn.itemDefinition(i)

            # Temp simplification
            if item_def.advanceLevel() > 0:
                print('Skipping advanced item {}'.format(item_def.name()))
                continue

            item_type = item_def.type()
            value_string = self.value_type_map.get(item_type)

            # Check item type
            if value_string is not None:
                param_dict = self._convert_value_type(item_def, value_string)
            elif item_type == smtk.attribute.Item.VoidType:
                param_dict = dict(
                    id=item_def.name(),
                    type='bool',
                    size=1,
                    label=item_def.label(),
                    ui='checkbox',
                    default=item_def.isEnabledByDefault())
            elif item_type == smtk.attribute.Item.GroupType:
                if item_def.numberOfRequiredGroups() > 1:
                    print('Skipping definition {} => group item {} because number of groups is {} not 1'.format(
                        defn.type(), item_def.name(), item_def.numberOfGroups()
                    ))
                elif item_def.isExtensible():
                    print('Skipping definition {} => group item {} because it is extensible'.format(
                        defn.type(), item_def.name(), item_def.numberOfGroups()
                    ))
                else:
                    show_condition = None
                    if item_def.isOptional():
                        param_dict = dict(
                            id=item_def.name(),
                            type='bool',
                            size=1,
                            label=item_def.label(),
                            ui='checkbox',
                            default=item_def.isEnabledByDefault())
                        param_list.append(param_dict)
                        show_condition = '{}[0]'.format(item_def.name())
                    # Flatten the group
                    param_list += self._convert_definition(item_def, show=show_condition)
                continue
            else:
                print('Skipping definition {} => item {} type {}'.format(defn.type(), item_def.name(), item_def.type()))
                continue

            brief = item_def.briefDescription()
            if brief:
                current_help = param_dict.get('help', '')
                param_dict['help'] = '{} {}'.format(brief, current_help)

            if show is not None:
                param_dict['show'] = show
            param_list.append(param_dict)

        return param_list

    def _convert_definitions(self):
        """Converts attribute definitions to simput definitions."""
        simput_defs = dict()
        defn_list = self.att_resource.definitions()
        for defn in defn_list:
            print('Converting attribute definition {}'.format(defn.type()))
            param_list = self._convert_definition(defn)
            member = dict(label=defn.label(), parameters=param_list)
            simput_defs[defn.type()] = member
        self.simput_model['definitions'] = simput_defs

    def _convert_value_type(self, item_def, value_string):
        """Converts value item definition to simput parameter (dict)."""
        param_size = 1  # default
        if hasattr(item_def, 'isExtensible') and item_def.isExtensible():
            param_size = -1
        elif hasattr(item_def, 'numberOfRequiredValues'):
            param_size = item_def.numberOfRequiredValues()
        param_dict = dict(
            id=item_def.name(),
            type=value_string,
            size=param_size,
            label=item_def.label())
        if item_def.hasDefault():
            default_list = [None] * param_size
            for i in range(param_size):
                default_list[i] = item_def.defaultValue(i)
            param_dict['default'] = default_list
        if item_def.isDiscrete():
            param_dict['ui'] = 'enum'
            choice_dict = dict()
            for i in range(item_def.numberOfDiscreteValues()):
                discrete_val = item_def.discreteValue(i)
                discrete_enum = item_def.discreteEnum(i)
                choice_dict[discrete_enum] = discrete_val
            param_dict['domain'] = choice_dict

        # Copy range info into "help"
        if item_def.hasRange():
            minside = '[-inf'
            maxside = '+inf]'
            if item_def.hasMinRange():
                symbol = '[' if item_def.minRangeInclusive() else '('
                minside = '{}{}'.format(symbol, item_def.minRange())
            if item_def.hasMaxRange():
                symbol = ']' if item_def.maxRangeInclusive() else ')'
                maxside = '{}{}'.format(item_def.maxRange(), symbol)
            range_string = '{}, {}'.format(minside, maxside)
            param_dict['help'] = range_string

        return param_dict

    def _convert_view(self, view, top_level=False):
        """Recursively generates simput views from smtk view."""
        view_type = view.type()
        view_comp = view.details()
        if view_type == 'Group':
            index = view_comp.findChild('Views')
            assert index >= 0, 'Group View has no \"Views\" component'
            views = view_comp.child(index)
            title_list = list()
            for i in range(views.numberOfChildren()):
                child = views.child(i)
                child_title = child.attributes().get('Title')
                title_list.append(child_title)
            # If this view is top level, only use it to set view order
            # Otherwise create simput view with children.
            if top_level:
                self.simput_model['order'] = title_list
            else:
                group_view = dict(label=view.name(), children=title_list)
                self.simput_views[view.name()] = group_view

            for title in title_list:
                child_view = self.att_resource.findView(title)
                self._convert_view(child_view)

        elif view_type in self.view_type_map:
            # Use same logic for Instanced and Attribute view types
            target = self.view_type_map.get(view_type)
            index = view_comp.findChild(target)
            atts_comp = view_comp.child(index)
            att_type_list = list()
            for i in range(atts_comp.numberOfChildren()):
                child_comp = atts_comp.child(i)
                att_type = child_comp.attributes().get('Type')
                att_type_list.append(att_type)
            new_view = dict(label=view.name(), attributes=att_type_list)
            if view_type == 'Attribute':
                new_view['size'] = -1
                # Add name parameter
                self._add_name_parameter(att_type_list, new_view)
            self.simput_views[view.name()] = new_view
        else:
            raise Exception('Unsupported view type {}'.format(view_type))

    def _convert_views(self):
        """Converts attribute views to simput views."""
        top_view = self.att_resource.findTopLevelView()
        if top_view is None:
            print('WARNING: SMTK Template missing TopLevel view')
            return
        self._convert_view(top_view, top_level=True)
        self.simput_model['views'] = self.simput_views


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Generate Simput model from SMTK attribute template')
    parser.add_argument('template_filepath', help='SMTK attribute template filename/path (.sbt)')
    parser.add_argument('-o', '--output_filepath', default='model.json', help='model filename/path (model.json)')

    args = parser.parse_args()

    # Import smtk *after* argparse so we can run witout smtk modules
    import smtk
    import smtk.attribute
    import smtk.io
    import smtk.view

    # Import SMTK template
    att_resource = smtk.attribute.Resource.create()
    reader = smtk.io.AttributeReader()
    logger = smtk.io.Logger.instance()
    print('Loading attribute file {}'.format(args.template_filepath))
    err = reader.read(att_resource, args.template_filepath, True, logger)
    assert err == False, 'Error reading attribute file {}'.format(args.template_filepath)

    # Initialize adapter
    adapter = Sbt2Simput()
    model = adapter.convert(att_resource)
    wrote = False
    with open(args.output_filepath, 'w') as fp:
        json.dump(model, fp, sort_keys=True, indent=2)
        fp.write('\n')
        wrote = True
        print('Wrote file', args.output_filepath)
    assert wrote, 'Failed to write model file {}'.format(args.output_filepath)
    # print('Simput model:')
    # print(json.dumps(model, sort_keys=True, indent=2))
