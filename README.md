# Docker images

Directory containing all our docker image definitions.

## debug

This image aims to be used for debugging the development image to
allow to build each step individually and incrementaly.

__Build image__

```
docker image build ./docker/debug -t py-smtk-debug
```

__Use image to build libraries__

```
mkdir -p docker-work

docker run --rm                          \
  -p 8888:8888                            \
  --entrypoint /bin/bash                     \
  -v $PWD/docker-work:/work:rw                \
  -v $PWD/docker/debug/scripts:/scripts        \
  -v $PWD/notebooks:/home/jovyan/notebooks:rw   \
  -it py-smtk-debug
```

From the docker you can execute each script to validate each step while
allowing incremental execution. This is only required when trying to
improve the development machine and capture properly every required
step.

```
/scripts/01-git.sh
/scripts/02-build-vtk.sh
/scripts/03-build-cmb.sh
/scripts/04-build-smtk.sh
/scripts/05-install.sh
/scripts/06-start-notebook.sh
```

## development

This image build VTK/CMB/SMTK using its development environment and will be
mandatory for the `runtime` image.

```
docker image build ./docker/development -t py-smtk-development
```

You can also run it via the development image like that

```
docker run -it --rm                           \
  -p 8888:8888                                 \
  -v ${PWD}/notebooks:/home/jovyan/notebooks:rw \
  py-smtk-development
```

## runtime

This image use the `development` image to copy the compiled version of
VTK and SMTK into the system so it can be used inside Jupyter.

```
docker image build ./docker/runtime -t py-smtk-runtime
```

# Running example using runtime

```
docker run -it --rm                           \
  -p 8888:8888                                 \
  -v ${PWD}/notebooks:/home/jovyan/notebooks:rw \
  py-smtk-runtime
```
