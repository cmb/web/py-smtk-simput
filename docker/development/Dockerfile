ARG BASE_CONTAINER=jupyter/minimal-notebook
FROM $BASE_CONTAINER

USER root

RUN apt-get update && apt-get install --yes \
    build-essential \
    ninja-build \
    libosmesa6-dev \
    curl \
    libarchive-dev \
    libboost-iostreams-dev \
    libboost-filesystem-dev \
    libboost-log-dev \
    libboost-program-options-dev \
    libboost-system-dev \
    libboost-timer-dev \
    libboost-thread-dev \
    libgdal-dev \
    liblas-dev \
    liblas-c3 \
    liblas-c-dev

# -----------------------------------------------------------------------------
# Install CMake 3.17
# -----------------------------------------------------------------------------

RUN mkdir -p /opt/cmake && cd /opt/cmake && \
    curl -L https://cmake.org/files/v3.17/cmake-3.17.0-Linux-x86_64.tar.gz | \
    tar --strip-components=1 -xzv

ENV CMAKE="/opt/cmake/bin/cmake"

# -----------------------------------------------------------------------------
# Working directory
# -----------------------------------------------------------------------------

ENV WORKDIR="$PWD"

# -----------------------------------------------------------------------------
# VTK/release
# -----------------------------------------------------------------------------

RUN mkdir -p "$WORKDIR/VTK/build" && cd "$WORKDIR/VTK" && \
    git clone https://gitlab.kitware.com/VTK/vtk.git src && \
    cd "$WORKDIR/VTK/src" && \
    git checkout release && \
    git submodule update --init --recursive

# -----------------------------------------------------------------------------
# CMB/sha
# -----------------------------------------------------------------------------

ENV CMB_SHA="6c38b623e6ead3730104b289be1426c31f359fe4"

RUN mkdir -p "$WORKDIR/CMB/build" && cd "$WORKDIR/CMB" && \
    git clone https://gitlab.kitware.com/cmb/cmb-superbuild.git src && \
    cd "$WORKDIR/CMB/src" && \
    git checkout $CMB_SHA && \
    git submodule update --init

# -----------------------------------------------------------------------------
# SMTK/master
# -----------------------------------------------------------------------------

ENV SMTK_MASTER="59500d0ba2d49d8612de63e8b18c826890fd9022"

RUN mkdir -p "$WORKDIR/SMTK/build" && cd "$WORKDIR/SMTK" && \
    git clone https://gitlab.kitware.com/cmb/smtk.git src && \
    cd "$WORKDIR/SMTK/src" && \
    git checkout $SMTK_MASTER && \
    git submodule update --init

# -----------------------------------------------------------------------------
# Build VTK
# -----------------------------------------------------------------------------

RUN $CMAKE \
      -S ${WORKDIR}/VTK/src \
      -B ${WORKDIR}/VTK/build \
      -G Ninja \
      -D CMAKE_BUILD_TYPE:STRING=Release \
      -D BUILD_TESTING:BOOL=OFF \
      -D BUILD_SHARED_LIBS:BOOL=ON \
      -D VTK_ENABLE_KITS:BOOL=OFF \
      -D VTK_OPENGL_HAS_OSMESA:BOOL=ON \
      -D VTK_DATA_EXCLUDE_FROM_ALL:BOOL=ON \
      -D VTK_DEFAULT_RENDER_WINDOW_OFFSCREEN:BOOL=ON \
      -D VTK_USE_X:BOOL=OFF \
      -D VTK_MODULE_ENABLE_VTK_FiltersOpenTURNS:STRING=NO \
      -D VTK_MODULE_ENABLE_VTK_IOADIOS2:STRING=NO \
      -D VTK_MODULE_ENABLE_VTK_CommonArchive:STRING=YES \
      -D VTK_MODULE_ENABLE_VTK_RenderingOpenVR:STRING=NO \
      -D VTK_MODULE_ENABLE_VTK_RenderingOpenGL2:STRING=YES \
      -D VTK_MODULE_ENABLE_VTK_GeovisCore:STRING=YES \
      -D VTK_MODULE_ENABLE_VTK_IOGDAL:STRING=YES \
      -D VTK_MODULE_ENABLE_VTK_IOLAS:STRING=YES \
      -D VTK_MODULE_ENABLE_VTK_IOParallelExodus:STRING=YES \
      -D VTK_MODULE_ENABLE_VTK_RenderingContextOpenGL2:STRING=YES \
      -D VTK_MODULE_ENABLE_VTK_RenderingGL2PSOpenGL2:STRING=YES \
      -D VTK_MODULE_ENABLE_VTK_RenderingMatplotlib:STRING=YES \
      -D VTK_MODULE_ENABLE_VTK_RenderingQt:STRING=NO \
      -D VTK_MODULE_ENABLE_VTK_TestingRendering:STRING=YES \
      -D VTK_MODULE_ENABLE_VTK_ViewsInfovis:STRING=YES \
      -D VTK_WRAP_PYTHON:BOOL=ON \
      -D VTK_GROUP_ENABLE_Web:BOOL=YES \
      -D OSMESA_LIBRARY=/usr/lib/x86_64-linux-gnu/libOSMesa.so \
      -D OSMESA_INCLUDE_DIR=/usr/include/GL/ \
      -D VTK_PYTHON_VERSION:STRING=3

RUN $CMAKE --build "$WORKDIR/VTK/build"

# -----------------------------------------------------------------------------
# Build CMB
# -----------------------------------------------------------------------------

RUN $CMAKE \
      -S ${WORKDIR}/CMB/src \
      -B ${WORKDIR}/CMB/build \
      -G Ninja \
      -D DEVELOPER_MODE_smtk:BOOL=ON \
      -D ENABLE_DOCUMENTATION:BOOL=OFF \
      -D ENABLE_cmb:BOOL=OFF \
      -D ENABLE_cmbusersguide:BOOL=OFF \
      -D ENABLE_qt5:BOOL=OFF \
      -D ENABLE_smtkprojectmanager:BOOL=OFF \
      -D ENABLE_smtkresourcemanagerstate:BOOL=OFF \
      -D ENABLE_smtkusersguide:BOOL=OFF \
      -D USE_SYSTEM_boost:BOOL=ON \
      -D USE_SYSTEM_PNG:BOOL=ON \
      -D USE_SYSTEM_python:BOOL=ON \
      -D ENABLE_python3:BOOL=ON \
      -D BUILD_TESTING:BOOL=OFF \
      -D USE_SYSTEM_zlib:BOOL=ON \
      -D TEST_smtk:BOOL=OFF \
      -D JSON_BuildTests=OFF \
      -D PYTHON_EXECUTABLE=/opt/conda/bin/python \
      -D PYTHON_INCLUDE_DIR=/opt/conda/include/python3.7m \
      -D PYTHON_LIBRARY=/opt/conda/lib/libpython3.7m.so \
      -D __BUILDBOT_INSTALL_LOCATION=/opt/cmb_superbuild

RUN $CMAKE --build "$WORKDIR/CMB/build"

# -----------------------------------------------------------------------------
# Build SMTK
# -----------------------------------------------------------------------------

RUN $CMAKE \
      -S ${WORKDIR}/SMTK/src \
      -B ${WORKDIR}/SMTK/build \
      -G Ninja \
      -C "$WORKDIR/CMB/build/smtk-developer-config.cmake" \
      -D "VTK_DIR=$WORKDIR/VTK/build" \
      -D BOOST_LIBRARYDIR=/usr/lib/x86_64-linux-gnu \
      -D SMTK_PYTHON_VERSION:STRING="3" \
      -D SMTK_ENABLE_QT_SUPPORT:BOOL=OFF \
      -D SMTK_ENABLE_TESTING:BOOL=OFF \
      -D SMTK_ENABLE_VTK_SUPPORT:BOOL=ON

RUN $CMAKE --build "$WORKDIR/SMTK/build"

# -----------------------------------------------------------------------------
# Install VTK + SMTK
# -----------------------------------------------------------------------------

RUN $CMAKE --install "$WORKDIR/VTK/build" --prefix "/opt/vtk"
RUN $CMAKE --install "$WORKDIR/SMTK/build" --prefix "/opt/smtk"

# -----------------------------------------------------------------------------
# Back to Jupyter user
# -----------------------------------------------------------------------------

ENV LD_LIBRARY_PATH=/opt/vtk/lib:/opt/smtk/lib:/opt/cmb_superbuild/lib
ENV PYTHONPATH=/opt/smtk/lib/python3.7/site-packages:opt/vtk/lib/python3.7/site-packages

ENV NB_USER=jovyan
ENV JUPYTER_ENABLE_LAB=yes

USER $NB_UID
