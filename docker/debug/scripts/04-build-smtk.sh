#!/bin/bash

export WORKDIR=/work

cd "$WORKDIR/smtk/build" && \
    $CMAKE \
      -S ${WORKDIR}/SMTK/src \
      -B ${WORKDIR}/SMTK/build \
      -G Ninja \
      -C "$WORKDIR/CMB/build/smtk-developer-config.cmake" \
      -D "VTK_DIR=$WORKDIR/VTK/build" \
      -D BOOST_LIBRARYDIR=/usr/lib/x86_64-linux-gnu \
      -D SMTK_PYTHON_VERSION:STRING="3" \
      -D SMTK_ENABLE_QT_SUPPORT:BOOL=OFF \
      -D SMTK_ENABLE_TESTING:BOOL=OFF \
      -D SMTK_ENABLE_VTK_SUPPORT:BOOL=ON

$CMAKE --build "$WORKDIR/smtk/build"

