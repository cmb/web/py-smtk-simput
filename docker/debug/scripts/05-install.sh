#!/bin/bash

export WORKDIR=/work

$CMAKE --install "$WORKDIR/VTK/build" --prefix /opt/vtk
$CMAKE --install "$WORKDIR/smtk/build" --prefix /opt/smtk
